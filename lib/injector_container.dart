import 'package:flutter_tul_cart/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:flutter_tul_cart/features/catalog/data/datasources/remote/firebase_dao/firebase_cart_dao.dart';
import 'package:flutter_tul_cart/features/catalog/data/datasources/remote/firebase_dao/firebase_products_dao.dart';
import 'package:flutter_tul_cart/features/catalog/data/repositories/shopping_repository.dart';
import 'package:flutter_tul_cart/features/catalog/presentation/bloc/catalog_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //
  //Localizador de servicios Blo
  sl.registerFactory(() => CatalogBloc(shoppingRepository: sl()));
  sl.registerFactory(() => CartBloc(shoppingRepository: sl()));

  sl.registerLazySingleton<IFireCartDao>(() => FireCartDao());
  sl.registerLazySingleton<IFireStoreProductsDao>(() => FireStoreProductsDao());
  sl.registerLazySingleton(() =>
      ShoppingRepository(iFireCartsDao: sl(), iFireStoreProductsDao: sl()));
}
