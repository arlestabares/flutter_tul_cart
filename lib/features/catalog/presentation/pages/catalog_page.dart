import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tul_cart/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/products.dart';
import 'package:flutter_tul_cart/features/catalog/presentation/bloc/catalog_bloc.dart';

class CatalogPage extends StatelessWidget {
  const CatalogPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ColoredBox(
        color: Colors.black.withOpacity(0.1),
        child: CustomScrollView(
          slivers: [
            const CatalogAppBar(),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 20,
                child: Container(
                ),
              ),
            ),
            BlocBuilder<CatalogBloc, CatalogState>(builder: (contex, state) {
              if (state is CatalogLoading) {
                return const SliverFillRemaining(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              if (state is CatalogLoaded) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) => CatalogListItem(
                      product: state.catalogProducts.getByPosition(index),
                    ),
                    childCount: state.catalogProducts.items.length
                  ),
                );
              }
              return const SliverFillRemaining(
                child: Text('Algo salio mal'),
              );
            })
          ],
        ),
      ),
    );
  }
}

class CatalogListItem extends StatelessWidget {
  final Product product;
  const CatalogListItem({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme.headline6;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: LimitedBox(
        maxHeight: 50.0,
        child: Row(
          children: <Widget>[
            Expanded(child: Text(product.name!, style: textTheme)),
            const SizedBox(width: 25),
            AddButton(product: product),
          ],
        ),
      ),
    );
  }
}

class AddButton extends StatelessWidget {
  final Product product;
  const AddButton({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        if (state is CartLoading) {
          return const CircularProgressIndicator();
        }
        if (state is CartLoaded) {
          final isInCart = state.cart.products!.contains(product);
          // final productCarts = state.
          return TextButton(
            onPressed: isInCart
                ? null
                : () => context.read<CartBloc>().add(CartItemAdded(product: product)),
            child: isInCart ? const Icon(Icons.check) : const Icon(Icons.add_circle),
          );
        }

        return const Text('Algo salio mal');
      },
    );
  }
}

class CatalogAppBar extends StatelessWidget {
  const CatalogAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      title: const Text('Catalog'),
      floating: true,
      actions: [
        IconButton(
          icon: const Icon(Icons.shopping_cart),
          onPressed: () => Navigator.pushNamed(context, 'cartPage'),
        )
      ],
    );
  }
}
