part of 'catalog_bloc.dart';

abstract class CatalogState extends Equatable {
  const CatalogState();

  @override
  List<Object> get props => [];
}

class CatalogLoading extends CatalogState {}

class CatalogLoaded extends CatalogState {
  //
  final CatalogProducts catalogProducts;
  //
  const CatalogLoaded(this.catalogProducts);

  @override
  List<Object> get props => [catalogProducts];
}

class CatalogError extends CatalogState {}
