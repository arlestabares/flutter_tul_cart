import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/catalog_products.dart';
import 'package:flutter_tul_cart/features/catalog/data/repositories/shopping_repository.dart';

part 'catalog_event.dart';
part 'catalog_state.dart';

class CatalogBloc extends Bloc<CatalogEvent, CatalogState> {
  //
  final ShoppingRepository shoppingRepository;

  CatalogBloc({required this.shoppingRepository}) : super(CatalogLoading()) {
    on<CatalogStarted>(_onStarted);
  }

  void _onStarted(CatalogStarted event, Emitter<CatalogState> emit) async {
    emit(CatalogLoading());

    try {
      final catalog = await shoppingRepository.loadCatalog();
      emit(CatalogLoaded(CatalogProducts(items: catalog)));
    } catch (_) {
      emit(CatalogError());
    }
  }
}
