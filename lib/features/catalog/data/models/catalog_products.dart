import 'package:equatable/equatable.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/products.dart';

class CatalogProducts extends Equatable {
  final List<String> items;

  const CatalogProducts({
    required this.items ,
  });

  Product getById(int id) => Product(id: id, name: items[id % items.length]);

  Product getByPosition(int position) => getById(position);

  @override
  List<Object?> get props => [items];
}
