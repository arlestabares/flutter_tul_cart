import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class Product extends Equatable {
  final int? id;
  final String? name;
  final String? sku;
  final String? description;

  const Product({
    this.id,
    this.name,
    this.sku,
    this.description,
  });

//Transformamos el JSON que se recibe del Cloud Firestore en un Product
  factory Product.fromJson(Map<dynamic, dynamic> json) => Product(
        id: json['id'] as int,
        name: json['name'] as String,
        sku: json['sku']as String,
        description: json['description']as String,
      );

  factory Product.snapshot(DocumentSnapshot snapshot) {
    final productSnapshot =
        Product.fromJson(snapshot.data() as Map<String, dynamic>);
    return productSnapshot;
  }

  @override
  List<Object?> get props => [id, name, sku, description];
}
