import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_tul_cart/features/cart/data/models/carts.dart';
import 'package:flutter_tul_cart/features/cart/data/models/product_carts.dart';

abstract class IFireCartDao {
  addCartsFirebase(Carts carts);
  addCartsStream(Carts carts);
  addProductCarts(ProductCarts productCarts);
}

class FireCartDao implements IFireCartDao {
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('carts');

  @override
  addCartsFirebase(Carts carts) {
    collection.add(carts.toJson());
  }

  @override
  addCartsStream(Carts carts) {
    collection.snapshots();
  }

  @override
  addProductCarts(ProductCarts productCarts) {
    collection.add(productCarts.toJson());
  }
}
