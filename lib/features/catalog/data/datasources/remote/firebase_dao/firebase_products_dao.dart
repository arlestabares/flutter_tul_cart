// FirebaseProductsDao realizara la funcion de: recuperar los datos de firestore
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/products.dart';

abstract class IFireStoreProductsDao {
  Future<dynamic> getProductsStreamList();
  Stream<List<String>> getProductsStreamLista();
}

// CartsDao realizara la funcion de: recuperar los datos
class FireStoreProductsDao implements IFireStoreProductsDao {
//Obtenemos una instancia de FirebaseFirestore y luego obtiene la raíz de la colección
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('products');

//Obtenemos los datos como un flujo desde el nivel raíz, y los retornamos como una lista
  @override
  Future<dynamic> getProductsStreamList() async{
    var completer = Completer();
    collection.get().then((value) {
     completer.complete(value.docs.map((e) =>e.get('name')).toList()) ;

    });
    return completer.future;
  }
 @override
  Stream<List<String>> getProductsStreamLista() {
    List<String> listaProducts = [];

    return collection.snapshots().map((snapshot) {
      var listaProducts =
          snapshot.docs.map((doc) => Product.snapshot(doc).name!).toList();
      return listaProducts;
    });
  }
}
