import 'package:flutter_tul_cart/features/catalog/data/datasources/remote/firebase_dao/firebase_cart_dao.dart';
import 'package:flutter_tul_cart/features/catalog/data/datasources/remote/firebase_dao/firebase_products_dao.dart';
import 'package:flutter_tul_cart/features/cart/data/models/carts.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/products.dart';

const _delay = Duration(milliseconds: 800);
const _productsName = [
  'Martillo',
  'Caladora',
  'Destornillador estrella',
  'Carretilla',
  'Alicate',
  'Pala Cuadrada',
  'Taladro',
  'Disco de pulir',
  'Pulidora',
  'Llave de grifo',
  'Bombillas Led',
  'Cinta Metrica',
  'Cinta de pegar',
  'destornillador pala',
  'Llave expansiva',
  'Adaptador de corriente',
];

class ShoppingRepository {
  var _productString = <String>[];
  var _product = <Product>[];

  final IFireCartDao iFireCartsDao;
  final IFireStoreProductsDao iFireStoreProductsDao;

  ShoppingRepository({
    required this.iFireStoreProductsDao,
    required this.iFireCartsDao,
  });

  Future<List<Product>> loadCartItems() =>
      Future.delayed(_delay, () => _product);

  void addItemToCart(Product item) => _product.add(item);

  void removeItemFromCart(Product item) => _product.remove(item);

  saveCartsInFirebase(Carts carts) {
    iFireCartsDao.addCartsFirebase(carts);
  }

  saveCartsStream(Carts carts) {
    return iFireCartsDao.addCartsStream(carts);
  }

  Future<List<String>> loadCatalog() {
    return Future.delayed(_delay, getStreamToListString);
  }

//Retorna los names de firebase como una lista de String
  Future<List<String>> getStreamToListString() async {
    List<String> lista = [];
    var result = iFireStoreProductsDao.getProductsStreamList();
    await result.then((value) {
      var productList = value as List<dynamic>;
      lista = productList.map((e) => e.toString()).toList();
    });
    return lista;
  }

//   Future<List<Product>> getStreamToProductList() async {
//     var listStream = iFireStoreProductsDao.getProductsStreamList();

//     await listStream.forEach((element) {
//       element.map((element) {
//         _product.add(element);
//       });
//     });
//     return _product;
//   }
// }
}
