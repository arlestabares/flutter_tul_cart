import 'package:flutter/material.dart';
import 'package:flutter_tul_cart/features/cart/presentation/pages/cart_page.dart';
import 'package:flutter_tul_cart/features/catalog/presentation/pages/catalog_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'catalogPage': (_) => const CatalogPage(),
  'cartPage': (_) => const CartPage(),
};
