import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_tul_cart/features/cart/data/models/carts.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/products.dart';

class ProductCarts extends Equatable {
  final int? quantity;
  final Carts? cartId;
  final Product? productId;
  final List<Product>? products;

  const ProductCarts({
     this.quantity,
     this.cartId,
     this.productId,
     this.products,
  });

  @override
  List<Object?> get props => [quantity,cartId,productId,products];

//Transformamos el JSON que se recibe del Cloud Firestore en un ProductsCart
  factory ProductCarts.fromJson(Map<dynamic, dynamic> json) => ProductCarts(
      quantity: json['quantity'] as int,
      cartId: json['cartId'] as Carts,
      productId: json['productId'] as Product);

  //Convertimos un ProductsCart en un JSON para subirlo a firestore
  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'quantity': quantity,
        'cartId': cartId,
        'productId': productId,
      };
//Tomamos una instantanea de firestore y la convertimos a ProductsCart
  factory ProductCarts.fromSnapshot(DocumentSnapshot snapshot) {
    final productsCartSnapshot =
        ProductCarts.fromJson(snapshot.data() as Map<dynamic, dynamic>);
    return productsCartSnapshot;
  }
}
