import 'package:cloud_firestore/cloud_firestore.dart';

enum Status {
  pending,
  completed,
}

class Carts {
  int id;
  Status status;

  Carts({required this.id, required this.status});

//Transformamos el JSON que se recibe del Cloud Firestore en un modelo de Carts
  factory Carts.fromJson(Map<dynamic, dynamic> json) => Carts(
        id: json['id'] as int,
        status: json['status'],
      );

//Convertimos el modelo de Carts en un JSON para subirlo a firestore
  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'id': id,
        'status': status,
      };

//Toma una instantánea de Firestore y la convierte en un modelo de Carts
  factory Carts.fromSnapshot(DocumentSnapshot snapshot) {
    final cartsSnapshot =
        Carts.fromJson(snapshot.data() as Map<dynamic, dynamic>);
    return cartsSnapshot;
  }
}
