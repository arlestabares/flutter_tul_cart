import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tul_cart/features/cart/presentation/bloc/cart_bloc.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ColoredBox(
        color: Colors.black.withOpacity(0.1),
        child: Column(
          children: const <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(32.0),
                child: CartList(),
              ),
            ),
            Divider(height: 3, color: Colors.black),
            CartFileButton()
          ],
        ),
      ),
    );
  }
}

class CartList extends StatelessWidget {
  const CartList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final itemStyle = Theme.of(context).textTheme.headline6;
    return BlocBuilder<CartBloc, CartState>(builder: (context, state) {
      if (state is CartLoading) {
        return const CircularProgressIndicator();
      }
      if (state is CartLoaded) {
        return ListView.separated(
          itemCount: state.cart.products!.length,
          separatorBuilder: (_, __) => const SizedBox(height: 4),
          itemBuilder: (context, index) {
            final product = state.cart.products![index];
            return Material(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
              clipBehavior: Clip.hardEdge,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CircleAvatar(
                    child: Text(product.name!.substring(0, 2)),
                  ),
                  const SizedBox(width: 10),
                  Expanded(child: Text(product.name!, style: itemStyle)),
                  IconButton(
                    icon: const Icon(Icons.remove_circle),
                    onPressed: () {
                      context.read<CartBloc>().add(CartItemRemoved(product));
                    },
                  ),
                  const Text(''),
                  IconButton(
                    icon: const Icon(Icons.add_circle),
                    onPressed: () {
                      context.read<CartBloc>().add(
                            CartItemAdded(product: product),
                          );
                    },
                  ),
                ],
              ),
            );
          },
        );
      }
      return const Text('Algo salio mal');
    });
  }
}

class CartFileButton extends StatelessWidget {
  const CartFileButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final itemStyle = Theme.of(context).textTheme.headline6;
    return SizedBox(
      height: 200.0,
      child: Center(
        child: Column(
          children: [
            BlocBuilder<CartBloc, CartState>(
              builder: (context, state) {
                if (state is CartLoading) {
                  return const CircularProgressIndicator();
                }
                if (state is CartLoaded) {
                  return const Text('');
                }
                return const Text('ALgo salio mal');
              },
            ),
            const SizedBox(width: 24),
            ElevatedButton(
              child: const Text('Save File'),
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Archivo generado')));
              },
            )
          ],
        ),
      ),
    );
  }
}
