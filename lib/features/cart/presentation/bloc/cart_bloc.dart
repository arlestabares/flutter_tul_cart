import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_tul_cart/features/cart/data/models/product_carts.dart';
import 'package:flutter_tul_cart/features/catalog/data/models/products.dart';
import 'package:flutter_tul_cart/features/catalog/data/repositories/shopping_repository.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  //
  final ShoppingRepository shoppingRepository;

  CartBloc({required this.shoppingRepository}) : super(CartLoading()) {
    on<CartStarted>(_onStared);
    on<CartItemAdded>(_onProductAdded);
    on<CartItemRemoved>(_onProductRemove);
  }

  void _onStared(CartStarted event, Emitter<CartState> emit) async {
    emit(CartLoading());
    try {
      final items = await shoppingRepository.loadCartItems();
      emit(CartLoaded(cart: ProductCarts(products: [...items])));
    } catch (_) {
      emit(CartError());
    }
  }

  void _onProductAdded(CartItemAdded event, Emitter<CartState> emit) async {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        shoppingRepository.addItemToCart(event.product!);
        emit(CartLoaded(
            cart: ProductCarts(
               //Realizo una desestructuracion de los items
                products: [...state.cart.products!, event.product!])));
      } catch (_) {
        emit(CartError());
      }
    }
  }

  void _onProductRemove(CartItemRemoved event, Emitter<CartState> emit) {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        shoppingRepository.removeItemFromCart(event.product);
        emit(CartLoaded(
            cart: ProductCarts(
                products: [...state.cart.products!]..remove(event.product))));
      } catch (_) {
        emit(CartError());
      }
    }
  }
}
