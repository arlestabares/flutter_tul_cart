import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tul_cart/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:flutter_tul_cart/features/catalog/data/repositories/shopping_repository.dart';
import 'package:flutter_tul_cart/features/catalog/presentation/bloc/catalog_bloc.dart';
import 'package:flutter_tul_cart/features/catalog/presentation/bloc_observer.dart';
import 'package:flutter_tul_cart/injector_container.dart' as di;
import 'package:flutter_tul_cart/features/routes/routes.dart';
import 'package:flutter_tul_cart/injector_container.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await di.init();
  Bloc.observer = SimpleBlocObserver();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) =>
                  CatalogBloc(shoppingRepository: sl<ShoppingRepository>())
                    ..add(CatalogStarted())),
          BlocProvider(
              create: (context) =>
                  CartBloc(shoppingRepository: sl<ShoppingRepository>())
                    ..add(CartStarted()))
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Material App',
          initialRoute: 'catalogPage',
          routes: appRoutes,
        ));
  }
}
